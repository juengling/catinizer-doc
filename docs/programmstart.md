# Programmstart

## Start via Desktop-Icon


## Start via Kommandozeile

Mit dem Aufruf `lernprogramm -h` oder `lernprogramm --help` erscheinen die Benutzungshinweise:

```
usage: Lizzy lernt schreiben v1.2 [-h] [-f FOLDER] [-l {1,2}] [--log-level LOGLEVEL]

Ein einfaches Lernprogramm für die Vorschule

options:
  -h, --help            show this help message and exit
  -f FOLDER, --folder FOLDER
                        Folder containing the files
  -l {1,2}, --level {1,2}
                        Set program level
  --log-level LOGLEVEL  Specify log level for console messages. LEVEL may be one of DEBUG, INFO, WARN, ERROR, CRITICAL (default: WARN).

```

Beim Start ohne Parameter geht das Programm davon aus, dass die Bilder im selben Verzeichnis wie die EXE-Datei liegen.

Soll ein anderes Verzeichnis genutzt werden, muss der Pfad angegeben werden:

    lernprogramm -f <path>
    lernprogramm --folder <path>
