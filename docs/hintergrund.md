# Hintergrund

Es fing alles mit einem Baby an, oder vielleicht sollte man *Kleinkind* sagen. Lizzy war damals etwa 1&nbsp;1/2 Jahre alt und erkundete intensiv ihre Umgebung. Sie tat das mit einer für dieses Alter bemerkenswerten Zielstrebigkeit, und war nicht nur lernfähig, sondern auch *lernwillig*.

Zu der besagten Umgebung zählten natürlich auch unsere Computer. So kam es, dass sie mit Windows Dinge anstellte, die wir uns niemals hätten träumen lassen. Nach kurzer Zeit bekam sie ihren eigenen Laptop -- einen ausrangierten mit Windows 7.

Dann kam meine Frau auf die Idee, ihr doch ein eigenes Programm zu schreiben, das auf ihre wilden Tastendrücke hin irgendwelche Bilder anzeigen sollte. Idealerweise Katzenbilder, denn Lizzy kommentierte jede Katze, die sie sah, mit einem lustigen Quietschen.

Das war die Geburtsstunde des *Catinizers*.