# Das Level-Konzept

Das Programm arbeitet mit mehreren Levels (aktuell nur 2), die jeweils für eine Erfahrungsstufe gedacht sind. Für eine Änderung des Levels ist gegenwärtig ein Neustart des Programms notwendig.

Die Auswahl eines Levels erfolgt beim Start über die Kommandozeile. Um das Level 2 auszuwählen:

    lernprogramm -l 2
    lernprogramm --level 2

Weitere Infos zur Kommandozeile gibt es im Kapitel [Programmstart](programmstart.md).

## Level 1
Ein beliebiger Tastendruck zeigt ein zufälliges Bild an, das bis zum nächsten Tastendruck stehen bleibt. Zusätzlich wird der getippte Buchstabe (groß geschrieben) angezeigt.

## Level 2
Nach dem Drücken eines Buchstabens wird aus allen Dateien, deren Name mit diesem Buchstaben beginnt, eine zufällig ausgewählt und angezeigt. Auch diese bleibt bis zum nächsten Tastendruck stehen.

Denkbar sind weitere Levels:

- **Level 3:** Wie Level 2, zusätzlich den Namen anzeigen und den Laut des Tieres ausgeben
- **Level 4:** Wie Level 3, zusätzlich den Namen als Audiodatei ausgeben, danach erst den Laut
- **Level 5:** Es muss der ganze Name getippt werden, dann wie Level 4
