# Lernprogramm

Ich habe immer noch keinen besseren Begriff dafür.

## Ziele

Dieses Programm soll Kinder ab dem Babyalter schrittweise in die Bedienung des PC einführen. Dabei sind zunächst keinerlei Kenntnisse gefragt, es geht lediglich darum zu erkennen, dass ein Druck auf eine Taste am Bildschirm eine Reaktion bewirkt. Später wird die Zuordnung Taste <--> Reaktion dann spezifischer werden.

Je nach Zusammenstellung der Bilder kann das Programm auch zum Lernen vonn Vokabeln nach Bildern verwendet werden.

## Zielgruppe und empfohlene Levels

Idee: Tabelle mit Alter und [Levels](levels.md).

To be done ...
